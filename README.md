# transportest

transportest enables you to fake HTTP responses in tests. It does so by provisioning an HTTP client with the Transport replaced with a stub.

The goal of this package is to encourage deep faking of test dependencies when the only reason for faking a dependency is to avoid external HTTP calls.
By faking the HTTP call only, you can avoid creating spurious fakes for your own code.

If you have ever extracted a procedure to an interface for injection in order to avoid external HTTP calls in test, and ended up with only one production implementation of that interface, this package is for you.

## Installation

To install transportest as a Go module, run the following command in your project root.

```sh
go get gitlab.com/humanlytyped/transportest
```

## Usage

See [examples_test.go](./examples_test.go) for the API usage examples. You can browse the tests in [transportest_test.go](./transportest_test.go) to understand how it works.
