// This file contains utility procedures that reduce duplication for common code.
// These would be found in the standard library or a utility library like github.com/samber/lo.

package transportest_test

import (
	"testing"

	"golang.org/x/sync/errgroup"
)

func todo(t *testing.T) {
	t.Skip("TODO!")
}

func tryTimes[T any](tries uint, f func(uint) (T, error)) ([]T, error) {
	result := make([]T, 0, tries)
	for i := uint(0); i < tries; i++ {
		t, err := f(i)
		if err != nil {
			return result, err
		}
		result = append(result, t)
	}
	return result, nil
}

func mapSlice[In, Out any](values []In, f func(In) Out) []Out {
	out := make([]Out, 0, len(values))
	for _, v := range values {
		out = append(out, f(v))
	}
	return out

}

func tryMap[In, Out any](values []In, f func(In) (Out, error)) ([]Out, error) {
	out := make([]Out, 0, len(values))
	for _, v := range values {
		result, err := f(v)
		if err != nil {
			return out, err
		}
		out = append(out, result)
	}
	return out, nil
}

func tryMapConcurrently[In, Out any](values []In, f func(In) (Out, error)) ([]Out, error) {
	out := make([]Out, len(values))
	g := &errgroup.Group{}
	for i, v := range values {
		i, v := i, v
		g.Go(func() error {
			result, err := f(v)
			out[i] = result
			return err
		})
	}
	err := g.Wait()
	return out, err
}

func groupBy[K comparable, V any](values []V, getKey func(V) K) map[K][]V {
	result := make(map[K][]V)
	for _, v := range values {
		key := getKey(v)
		if value, exists := result[key]; exists {
			result[key] = append(value, v)
			continue
		}
		result[key] = []V{v}
	}
	return result
}
