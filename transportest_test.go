package transportest_test

import (
	"cmp"
	"net/http"
	"slices"
	"strconv"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/humanlytyped/transportest"
)

func TestRecordRoundTrips(t *testing.T) {
	t.Run("returned roundtrips is initially empty", func(t *testing.T) {
		roundtrips, _ := transportest.RecordRoundTrips(nil)

		require.NotNil(t, roundtrips)
		require.Len(t, *roundtrips, 0)
	})

	t.Run("returned roundtrips is filled after a request is made on the http client", func(t *testing.T) {
		expectedResponse := &http.Response{}
		roundtrips, httpclient := transportest.RecordRoundTrips(transportest.FixedResponder(expectedResponse, nil))
		request, err := http.NewRequest(http.MethodGet, "http://example.com", nil)
		require.NoError(t, err)

		response, err := httpclient.Do(request)

		require.NoError(t, err)
		require.Len(t, *roundtrips, 1)
		require.Equal(t, expectedResponse, response)
		expectedRoundtrip := transportest.Roundtrip{Request: request, Response: expectedResponse, Error: nil}
		require.Equal(t, expectedRoundtrip, (*roundtrips)[0])
	})

	t.Run("maintains the order of sequential requests on the http client", func(t *testing.T) {
		var respond transportest.Responder = func(r *http.Request) (*http.Response, error) {
			response := &http.Response{Header: http.Header{}}
			response.Header.Set("x-count", r.URL.Query().Get("count"))
			return response, nil
		}
		requests, err := tryTimes(10, func(count uint) (*http.Request, error) {
			r, err := http.NewRequest(http.MethodGet, "http://example.com", nil)
			query := r.URL.Query()
			query.Set("count", strconv.Itoa(int(count)))
			r.URL.RawQuery = query.Encode()
			return r, err
		})
		require.NoError(t, err)
		roundtrips, httpclient := transportest.RecordRoundTrips(respond)

		_, err = tryMap(requests, httpclient.Do)
		require.NoError(t, err)

		require.Len(t, *roundtrips, len(requests))
		observedResponses := mapSlice(*roundtrips, getRoundtripResponse)
		require.Equal(t, 0, slices.CompareFunc(requests, observedResponses, func(r1 *http.Request, r2 *http.Response) int {
			r1Count := r1.URL.Query().Get("count")
			r2Count := r2.Header.Get("x-count")
			return cmp.Compare(r1Count, r2Count)
		}))
	})
}

func Fuzz_RecordRoundTrips_captures_all_concurrent_requests_correctly(f *testing.F) {
	f.Fuzz(func(t *testing.T, requestsCount uint8) {
		requests, err := tryTimes(uint(requestsCount), func(count uint) (*http.Request, error) {
			r, err := http.NewRequest(http.MethodGet, "http://example.com", nil)
			query := r.URL.Query()
			query.Set("count", strconv.Itoa(int(count)))
			r.URL.RawQuery = query.Encode()
			return r, err
		})
		require.NoError(t, err)
		roundtrips, httpclient := transportest.RecordRoundTrips(transportest.ClonedResponder(&http.Response{}, nil))

		_, err = tryMapConcurrently(requests, httpclient.Do)
		require.NoError(t, err)

		require.Len(t, *roundtrips, int(requestsCount))
		groupedByCountParam := groupBy(*roundtrips, recordedRequestQueryParam("count"))
		require.Len(t, groupedByCountParam, int(requestsCount))
	})
}

func recordedRequestQueryParam(param string) func(transportest.Roundtrip) string {
	return func(r transportest.Roundtrip) string {
		return r.Request.URL.Query().Get(param)
	}
}

func getRoundtripResponse(r transportest.Roundtrip) *http.Response {
	return r.Response
}
