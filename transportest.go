package transportest

import (
	"bytes"
	"io"
	"net/http"
	"sync"
)

type Roundtrip struct {
	Request  *http.Request
	Response *http.Response
	Error    error
}

type Responder func(*http.Request) (*http.Response, error)

// RoundTrip implements http.RoundTripper.
func (respondTo Responder) RoundTrip(request *http.Request) (*http.Response, error) {
	return respondTo(request)
}

var _ http.RoundTripper = Responder(nil)

type recordedSession struct {
	roundTripper http.RoundTripper
	roundtrips   []Roundtrip
	appendLock   *sync.Mutex
}

func newRecordedSession(respond Responder) *recordedSession {
	return &recordedSession{roundTripper: respond, appendLock: &sync.Mutex{}}
}

// RoundTrip implements http.RoundTripper.
func (rs *recordedSession) RoundTrip(request *http.Request) (*http.Response, error) {
	response, err := rs.roundTripper.RoundTrip(request)
	if response != nil {
		response.Request = request
	}
	roundtrip := Roundtrip{Error: err, Request: request, Response: response}
	rs.appendLock.Lock()
	rs.roundtrips = append(rs.roundtrips, roundtrip)
	rs.appendLock.Unlock()
	return response, err
}

var _ http.RoundTripper = &recordedSession{}

// RecordRoundTrips responds to HTTP requests made on client using respond.
// *roundtrips starts out empty and gets filled as requests are made on client.
// For sequential requests on client, roundtrips will be ordered by the requests.
// For concurrent requests on client, roundtrips has no defined order.
// The caller can observe intermediate states while requests are made on client.
// The caller is expected to only read roundtrips.
func RecordRoundTrips(respond Responder) (roundtrips *[]Roundtrip, client http.Client) {
	var rs *recordedSession = newRecordedSession(respond)
	return &rs.roundtrips, http.Client{Transport: rs}
}

// FixedResponder returns a RoundTripFunc that always returns (response, err).
// This is most useful for single roundtrips or multiple roundtrips where the response is unimportant.
func FixedResponder(response *http.Response, err error) Responder {
	return func(r *http.Request) (*http.Response, error) {
		return response, err
	}
}

// ClonedResponder returns a Responder that responds to each request with a fresh copy of response and err.
// ClonedResponder is suitable for situations where multiple requests will read their response bodies,
// so the body of response is copied for each request to ensure that each request gets the same body.
func ClonedResponder(response *http.Response, err error) Responder {
	if response == nil || response.Body == nil {
		return func(r *http.Request) (*http.Response, error) {
			response := *response
			return &response, err
		}
	}
	b, readError := io.ReadAll(response.Body)
	if readError != nil {
		panic(readError)
	}
	return func(r *http.Request) (*http.Response, error) {
		res := *response
		res.Body = io.NopCloser(bytes.NewReader(b))
		return &res, err
	}
}
