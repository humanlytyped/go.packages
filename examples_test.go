package transportest_test

import (
	"fmt"
	"io"
	"net/http"
	"strings"

	tt "gitlab.com/humanlytyped/transportest"
)

func ExampleSingleRequest() {
	response := &http.Response{StatusCode: http.StatusOK}
	roundtrips, httpclient := tt.RecordRoundTrips(tt.FixedResponder(response, nil))

	request, _ := http.NewRequest(http.MethodGet, "http://example.com", nil)
	_, _ = httpclient.Do(request)

	fmt.Printf("len(roundtrips) = %d\n", len(*roundtrips))
	fmt.Printf("request URL = %s\n", request.URL)
	fmt.Printf("captured request URL = %s\n", (*roundtrips)[0].Request.URL)

	// Output:
	// len(roundtrips) = 1
	// request URL = http://example.com
	// captured request URL = http://example.com
}

func ExampleMultipleRequestsReadingResponseBody() {
	responseText := "<span>response text</span>"
	response := &http.Response{
		StatusCode: http.StatusOK,
		Header:     http.Header{},
		Body:       io.NopCloser(strings.NewReader(responseText)),
	}
	response.Header.Set("Content-Type", "text/html")
	roundtrips, httpclient := tt.RecordRoundTrips(tt.ClonedResponder(response, nil))

	requests, err := tryTimes(5, func(u uint) (*http.Request, error) {
		return http.NewRequest(http.MethodGet, "http://example.com", nil)
	})
	mustNotError(err)

	responses, err := tryMap(requests, httpclient.Do)
	mustNotError(err)

	fmt.Printf("len(roundtrips) = %d\n", len(*roundtrips))
	for i, res := range responses {
		fmt.Printf("request URL %d = %s\n", i, res.Request.URL)
		fmt.Printf("captured request URL %d = %s\n", i, (*roundtrips)[i].Request.URL)
		b, err := io.ReadAll(res.Body)
		mustNotError(err)
		fmt.Printf("response body = %s\n", b)
	}

	// Output:
	// len(roundtrips) = 5
	// request URL 0 = http://example.com
	// captured request URL 0 = http://example.com
	// response body = <span>response text</span>
	// request URL 1 = http://example.com
	// captured request URL 1 = http://example.com
	// response body = <span>response text</span>
	// request URL 2 = http://example.com
	// captured request URL 2 = http://example.com
	// response body = <span>response text</span>
	// request URL 3 = http://example.com
	// captured request URL 3 = http://example.com
	// response body = <span>response text</span>
	// request URL 4 = http://example.com
	// captured request URL 4 = http://example.com
	// response body = <span>response text</span>
}

func mustNotError(err error) {
	if err != nil {
		panic(err)
	}
}
