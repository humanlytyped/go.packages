// transportest provides functions for verifying outgoing HTTP requests from a function.
// It does this by overriding the default transport with an stub that captures the outgoing request and responds however you choose.
package transportest
